from flask import Flask, jsonify, request,Blueprint

from api import app
from api import settings
from api.recomendacion_ranking.controlador import recomendacion_ranking
from api.recomendacion_arboles.controlador import recomendacion_arboles
from api.recomendacion.controlador import recomendacion

def configure_app(flask_app):
    # flask_app.config['SERVER_NAME'] = settings.FLASK_SERVER_NAME
    flask_app.config['SERVER_HOST'] = settings.FLASK_SERVER_HOST
    flask_app.config['SERVER_PORT'] = settings.FLASK_SERVER_PORT
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = settings.RESTPLUS_ERROR_404_HELP

    
def initialize_app(flask_app):
    configure_app(flask_app)
    flask_app.register_blueprint(recomendacion_arboles)
    flask_app.register_blueprint(recomendacion_ranking)
    flask_app.register_blueprint(recomendacion)

def main():
    initialize_app(app)
    app.run(host=settings.FLASK_SERVER_HOST, port=settings.FLASK_SERVER_PORT, debug=settings.FLASK_DEBUG)

if __name__ == "__main__":
    main()
