from keras.utils import to_categorical
import numpy
import pandas
from pandas.io.json import json_normalize
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from api.comunes.utilidades import Utilidades
from sklearn.preprocessing import MinMaxScaler

class Normalizacion():
    @staticmethod
    def normalizarCamposFechas(dataframe,campos):
        for campo in campos:
            dataframe[campo]=dataframe[campo].str.replace('-','').apply(int)
        return dataframe
        
    @staticmethod
    def normalizarCamposHoras(dataframe,campos):
        for campo in campos:
            dataframe[campo]=dataframe[campo].str.replace(':','').apply(int)
        return dataframe
    
    @staticmethod
    def normalizarCamposCategorias(dataframe,campos):
        onehotencoder = OneHotEncoder(categorical_features = [1])
        for campo in campos:
            categorias = dataframe[campo].unique()
            listaCategorias = []
            for dato in dataframe[campo]:
                listaCategorias.append(categorias.tolist().index(dato)+1)
            #categoricaConv=np_utils.to_categorical(listaCategorias)
            dataframe[campo] = listaCategorias
            #
        return dataframe
    
    @staticmethod
    def normalizarAtributosCategorias(dataframe,campos):
        for campo in campos:
            #dataframe[campo] = LabelEncoder().fit_transform(dataframe[campo])
            #dataframe[campo] = np_utils.to_categorical(dataframe[campo])
            dataframe = Normalizacion.create_dummies(dataframe,campo)
        return dataframe
    
    # An utility function to create dummy variable
    @staticmethod
    def create_dummies( df, colname ):
        print("Columna: "+colname)
        col_dummies = pandas.get_dummies(df[colname], prefix=colname)
        col_dummies.drop(col_dummies.columns[0], axis=1, inplace=True)
        df = pandas.concat([df, col_dummies], axis=1)
        df.drop( colname, axis = 1, inplace = True )
        return df
        
    @staticmethod
    def preProcesarAtributos(dataframe,campos):
        for campo in campos:
            scaler = MinMaxScaler()
            scaler.fit(dataframe)
            dataframe[campo] = scaler.transform(dataframe)
        return dataframe
    
    @staticmethod
    def preProcesarClases(dsCategorias):
        dsCategorias = np_utils.to_categorical(dsCategorias,num_classes = None)
        return dsCategorias
    
    @staticmethod
    def preprocessEtiquetas(labels):
        encoder = LabelEncoder()
        encoder.fit(labels)
        y = encoder.transform(labels)
        #y = np_utils.to_categorical(y)
        return y,encoder
    
    @staticmethod
    def transformarCategorias(categorias):
        listaCategoriasPredecidas = []
        for prediccion in categorias:
            categoriaPredecida = numpy.argmax(prediccion, axis=None, out=None)
            listaCategoriasPredecidas.append(categoriaPredecida)
        return listaCategoriasPredecidas
    
    @staticmethod
    def transformarColeccionToDataFrame(coleccion):
        dataframe = json_normalize(coleccion)
        return dataframe