import numpy
import pandas

class Utilidades():
    @staticmethod
    def agregarListaDataFrame(dataframe1,lista,nombreCampo):
        numeroRegistros = len(lista)
        #duplico el registro por el numero que tiene la lista
        dataframe =  pandas.concat([dataframe1]*numeroRegistros, ignore_index=True)

        #agrego la nueva columna
        dataframe[nombreCampo] = lista
        return dataframe