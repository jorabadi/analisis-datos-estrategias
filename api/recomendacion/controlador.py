import json
import bson
from numpy import array
import pandas as pd
from flask import jsonify, Blueprint,request
from api.recomendacion.logica import LogicaRecomendacion
from api.recomendacion.modelo import ModeloRecomendacion

recomendacion = Blueprint('recomendacion', __name__)

class ControladorRecomendacion():
       
    @staticmethod
    @recomendacion.route('/recomendacion/modelo', methods=['GET'])
    def crearModeloRecomendacion():
        dsEstrategias= ModeloRecomendacion.obtenerEjecucionEstrategias()
        modelo,estructuraAtributosModelo =  LogicaRecomendacion.crearModeloRecomendacion(dsEstrategias)
        ModeloRecomendacion.guardarModelo(modelo)
        return jsonify({'result':"ok"})
    
    
    @staticmethod
    @recomendacion.route('/recomendacion/estrategias', methods=['GET'])
    def recomendarEstrategias():
        dsEstrategias= ModeloRecomendacion.obtenerEjecucionEstrategias()
        modelo,estructuraAtributosModelo = LogicaRecomendacion.obtenerModeloRecomendacionGuardado(dsEstrategias)
        
        listaItems = dsEstrategias["estrategia"].unique()
        atributosPrediccion= ModeloRecomendacion.obtenerAtributosPrediccion()
        
        print(listaItems)
        estrategias = LogicaRecomendacion.recomendarEstrategiasTop(modelo,atributosPrediccion,listaItems,estructuraAtributosModelo)
        return jsonify({'result':"ok", "datos":estrategias})
    
    @staticmethod
    @recomendacion.route('/recomendacion/estrategias/mejores', methods=['POST'])
    def recomendarMejoresEstrategias():
        datos = json.loads(request.data)
        
        dsEstrategias= ModeloRecomendacion.obtenerEjecucionEstrategias()
        modelo,estructuraAtributosModelo = LogicaRecomendacion.obtenerModeloRecomendacionGuardado(dsEstrategias)
        
        listaItems = dsEstrategias["estrategia"].unique()
        
        atributosPrediccion = ControladorRecomendacion.obtenerAtributosPrediccionRequest(datos)
        
        estrategias = LogicaRecomendacion.recomendarEstrategiasTop(modelo,atributosPrediccion,listaItems,estructuraAtributosModelo)
        return jsonify({'result':"ok", "datos":estrategias})
        
    @staticmethod
    def obtenerAtributosPrediccionRequest(data):
        rTienda = ModeloRecomendacion.buscarTienda(data["codigo_proveedor"])
        
        data = array([[
            1,data["fecha_inicio"],data["fecha_fin"],data["temporada"],
            rTienda["rango_venta"].values[0],rTienda["ventas_ultimo_mes"].values[0],rTienda["ventas_penultimo_mes"].values[0],
            rTienda["crecimiento"].values[0],data["inversion_tiendas"],rTienda["mundo_pertenece"].values[0],
            rTienda["pines"].values[0],
            rTienda["tipo_tienda"].values[0],data["tipo_estrategias"]
        ]])
        
        columnas = ['id','fecha_inicio','fecha_fin','temporada','rango_ventas','ventas_ultimo_mes',
       'ventas_penultimo_mes','crecimiento_ventas','inversion_tiendas','mundo_pertenece','pines_mes',
        'tipos_productos','tipo_estrategias']
        dfAtributosPrediccion = pd.DataFrame.from_records(data=data,index=['id'],columns = columnas)
        return dfAtributosPrediccion
        
    @staticmethod
    @recomendacion.route('/recomendacion/tiendas/<nombre>', methods=['GET'])
    def listarTiendas(nombre=None):
        dsTiendas= ModeloRecomendacion.listarTiendas(nombre)
        return jsonify({'result':"ok", "datos":dsTiendas.to_dict('index')})
    
    @staticmethod
    @recomendacion.route('/recomendacion/tienda/<codigoTienda>', methods=['GET'])
    def buscarTienda(codigoTienda=None):
        dsTienda= ModeloRecomendacion.buscarTienda(codigoTienda)
        return jsonify({'result':"ok", "datos":dsTienda.to_dict('index')})