import pandas
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
from api.comunes.normalizacion import Normalizacion
from api.comunes.utilidades import Utilidades
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from matplotlib import pyplot
from keras import backend

import numpy as np
from scipy import interp
import matplotlib.pyplot as plt
from itertools import cycle
from sklearn.metrics import roc_curve, auc
from api.recomendacion.modelo import ModeloRecomendacion
from pandas_ml import ConfusionMatrix

class LogicaRecomendacion():
    path = "../datos/"
   
    @staticmethod
    def normalizarCamposEstrategias(dsAtributos):
        camposFecha = ["fecha_inicio","fecha_fin"]
        camposCategorias = ["temporada","mundo_pertenece","tipos_productos","tipo_estrategias","estrategia"]
        
        camposAtributos = ["rango_ventas","ventas_penultimo_mes","ventas_ultimo_mes",
        "crecimiento_ventas","inversion_tiendas","pines_mes","fecha_inicio","fecha_fin"]
        
        dsAtributos = Normalizacion.normalizarCamposFechas(dsAtributos,camposFecha)
        dsAtributos = Normalizacion.normalizarAtributosCategorias(dsAtributos,camposCategorias)
        dsAtributos = Normalizacion.preProcesarAtributos(dsAtributos,camposAtributos)
        return dsAtributos
    
    @staticmethod
    def crearModeloRecomendacion(dsEstrategias):
        dsEstrategias = LogicaRecomendacion.normalizarCamposEstrategias(dsEstrategias)
        
        #separo los atributos de las clases
        dsAtributosEstrategias  = dsEstrategias.drop('calificacion', axis=1)
        dsClasesEstrategias = dsEstrategias['calificacion']
        
        variablesSalida = int(dsClasesEstrategias.max() + 1)
        
       #la estructura del los campos cuales se realiza el aprendizaje
        estructuraAtributosModelo = dsAtributosEstrategias.columns
        variablesEntrada = len(estructuraAtributosModelo)
        
        dsClasesEstrategias = Normalizacion.preProcesarClases(dsClasesEstrategias)
        
        
        modelo = LogicaRecomendacion.crearModelo(variablesEntrada,variablesSalida)
        
        #divido el dataset en datos de entrenamiento y de prueba
        dsAtributosEntrenamiento, dsAtributosPrueba, dsClasesEntrenamiento, dsClasesPrueba = train_test_split(dsAtributosEstrategias, dsClasesEstrategias, test_size=0.30)
        
        history = LogicaRecomendacion.entrenarModelo(modelo,dsAtributosEntrenamiento,dsClasesEntrenamiento)

        y_score = LogicaRecomendacion.evaluarModelo(modelo,dsAtributosPrueba,dsClasesPrueba)
        
        LogicaRecomendacion.plotROC(dsClasesPrueba,y_score,variablesSalida)
        
        return modelo,estructuraAtributosModelo
        
    @staticmethod
    def crearModelo(numVarEntradas,numVarSalidas):
        #9 columnas = parametros de entrada
        #5 categorias = nodos de salida
        red = Sequential()
        red.add(Dense(numVarEntradas, input_dim = numVarEntradas, activation='relu'))
        red.add(Dropout(0.2))
        red.add(Dense(500,  activation='relu'))
        red.add(Dropout(0.2))
        red.add(Dense(numVarSalidas,  activation='softmax'))
        adam = Adam(lr=0.001)
        red.compile(loss= 'categorical_crossentropy', optimizer=adam, metrics = ['accuracy'])
        return red
    
    @staticmethod
    def rmse(y_true, y_pred):
        return backend.sqrt(backend.mean(backend.square(y_pred - y_true), axis=-1))

    @staticmethod
    def entrenarModelo(modelo,dsAtributosEntrenamiento,dsClasesEntrenamiento):
        history = modelo.fit(dsAtributosEntrenamiento,dsClasesEntrenamiento, epochs=100, batch_size=64,validation_data=(dsAtributosEntrenamiento, dsClasesEntrenamiento), verbose=2)
        
        # plot train and validation loss
        #pyplot.plot(history.history['loss'])
        #pyplot.plot(history.history['val_loss'])
        #pyplot.title('model train vs validation loss')
        #pyplot.ylabel('loss')
        #pyplot.xlabel('epoch')
        #pyplot.legend(['train', 'validation'], loc='upper right')
        #pyplot.savefig(LogicaRecomendacion.path+"overfitting.png")
        
        #pyplot.plot(history.history['acc'])
        #pyplot.savefig(LogicaRecomendacion.path+"precision.png")
        
        #pyplot.plot(history.history['rmse'])
        #pyplot.savefig(LogicaRecomendacion.path+"rmse.png")
        
        #pyplot.plot(history.history['mean_absolute_error'])
        #pyplot.savefig(LogicaRecomendacion.path+"mae.png")
        
        
    @staticmethod   
    def evaluarModelo(modelo,dsAtributosPrueba,dsClasesPrueba):
        #realizo la prediccion con los datos de pruebas
        dsClasesPrediccion = modelo.predict(dsAtributosPrueba)
        
        scores = modelo.evaluate(dsAtributosPrueba, dsClasesPrueba)
        print("%s: %.2f%%" % (modelo.metrics_names[1], scores[1]*100))
        
        dsClasesDesNormalizadasPrueba = Normalizacion.transformarCategorias(dsClasesPrueba)
        dsClasesDesNormalizadasPrediccion = Normalizacion.transformarCategorias(dsClasesPrediccion)

        cm = ConfusionMatrix(dsClasesDesNormalizadasPrueba, dsClasesDesNormalizadasPrediccion)
        cm.print_stats()
        #creo la matriz de confusion
        #print(confusion_matrix(dsClasesDesNormalizadasPrueba, dsClasesDesNormalizadasPrediccion))
        print(classification_report(dsClasesDesNormalizadasPrueba, dsClasesDesNormalizadasPrediccion))
        
        return dsClasesPrediccion
    
    @staticmethod
    def recomendarEstrategiasTop(modelo,atributosPrediccion,listaItems,estructuraAtributosModelo):
        dsEstrategias = Utilidades.agregarListaDataFrame(atributosPrediccion,listaItems,"estrategia")
        dsEstrategias = LogicaRecomendacion.normalizarCamposEstrategias(dsEstrategias)
        
        #creo un dataframe con la estructura del modelo del aprendizaje luego de la normalizacion
        dataframeResultante = pandas.DataFrame(columns=estructuraAtributosModelo)
        for columna in dsEstrategias.columns:
            dataframeResultante[columna] = dsEstrategias[columna]
        
        dataframeResultante = dataframeResultante.fillna(0)
        dsEstrategias = dataframeResultante
        
        topPredicciones = modelo.predict(dsEstrategias)
        
        topPredicciones = Normalizacion.transformarCategorias(topPredicciones)
        
        
        
        result = []
        indice = 0
        for prediccion in topPredicciones:
            result.append({'estrategia':listaItems[indice],'calificacion':prediccion})
            indice = indice + 1
            
        result = sorted(result, key=lambda k: k['calificacion'], reverse=True) 
        return result[:5]
    
    @staticmethod
    def plotROC(y_test,y_pred_keras,n_classes):
        
        # Plot linewidth.
        lw = 2
        
        # Compute ROC curve and ROC area for each class
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_pred_keras[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])
        
        # Compute micro-average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_pred_keras.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
        
        # Compute macro-average ROC curve and ROC area
        
        # First aggregate all false positive rates
        all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))
        
        # Then interpolate all ROC curves at this points
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += interp(all_fpr, fpr[i], tpr[i])
        
        # Finally average it and compute AUC
        mean_tpr /= n_classes
        
        fpr["macro"] = all_fpr
        tpr["macro"] = mean_tpr
        roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])
        
        # Plot all ROC curves
        plt.figure(1)
        plt.plot(fpr["micro"], tpr["micro"],
                 label='micro-average ROC curve (area = {0:0.2f})'
                       ''.format(roc_auc["micro"]),
                 color='deeppink', linestyle=':', linewidth=4)
        
        plt.plot(fpr["macro"], tpr["macro"],
                 label='macro-average ROC curve (area = {0:0.2f})'
                       ''.format(roc_auc["macro"]),
                 color='navy', linestyle=':', linewidth=4)
        
        colors = cycle(['aqua', 'darkorange', 'cornflowerblue'])
        for i, color in zip(range(n_classes), colors):
            plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                     label='ROC curve of class {0} (area = {1:0.2f})'
                     ''.format(i, roc_auc[i]))
        
        plt.plot([0, 1], [0, 1], 'k--', lw=lw)
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC')
        plt.legend(loc="lower right")
        plt.savefig(LogicaRecomendacion.path+"roc.png")
        
        # Zoom in view of the upper left corner.
        plt.figure(2)
        plt.xlim(0, 0.2)
        plt.ylim(0.8, 1)
        plt.plot(fpr["micro"], tpr["micro"],
                 label='micro-average ROC curve (area = {0:0.2f})'
                       ''.format(roc_auc["micro"]),
                 color='deeppink', linestyle=':', linewidth=4)
        
        plt.plot(fpr["macro"], tpr["macro"],
                 label='macro-average ROC curve (area = {0:0.2f})'
                       ''.format(roc_auc["macro"]),
                 color='navy', linestyle=':', linewidth=4)
        
        colors = cycle(['aqua', 'darkorange', 'cornflowerblue'])
        for i, color in zip(range(n_classes), colors):
            plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                     label='ROC curve of class {0} (area = {1:0.2f})'
                     ''.format(i, roc_auc[i]))
        
        plt.plot([0, 1], [0, 1], 'k--', lw=lw)
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Some extension of Receiver operating characteristic to multi-class')
        plt.legend(loc="lower right")
        plt.savefig(LogicaRecomendacion.path+"roc_zoom.png")
        
        
    @staticmethod
    def obtenerModeloRecomendacionGuardado(dsEstrategias):
        dsEstrategias = LogicaRecomendacion.normalizarCamposEstrategias(dsEstrategias)
        
        #separo los atributos de las clases
        dsAtributosEstrategias  = dsEstrategias.drop('calificacion', axis=1)
        dsClasesEstrategias = dsEstrategias['calificacion']
        
        variablesSalida = int(dsClasesEstrategias.max() + 1)
        
       #la estructura del los campos cuales se realiza el aprendizaje
        estructuraAtributosModelo = dsAtributosEstrategias.columns
        
        modelo = ModeloRecomendacion.obtenerModelo()
        return modelo,estructuraAtributosModelo