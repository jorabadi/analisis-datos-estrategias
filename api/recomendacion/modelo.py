import pandas as pd
from keras.models import model_from_json,load_model

class ModeloRecomendacion():
    path = "../datos_estrategias/"
    
    @staticmethod
    def obtenerEjecucionEstrategias():
       rutaArchivo = ModeloRecomendacion.path+"datos_entrenamiento.csv"
       return pd.read_csv(rutaArchivo,delimiter=",", index_col=0, decimal=",")
   
    @staticmethod
    def obtenerAtributosPrediccion():
       rutaArchivo = ModeloRecomendacion.path+"datos_prediccion.csv"
       return pd.read_csv(rutaArchivo,delimiter=",", index_col=0, decimal=",")
   
    @staticmethod
    def guardarModelo(modelo):
        rutaArchivoModeloHdf5 = ModeloRecomendacion.path+"modelo.h5"
        modelo.save(rutaArchivoModeloHdf5)
        
    @staticmethod
    def obtenerModelo():
        rutaArchivoModeloHdf5 = ModeloRecomendacion.path+"modelo.h5"
        modelo = load_model(rutaArchivoModeloHdf5)
        return modelo
    
    @staticmethod
    def listarTiendas(nombre):
        rutaArchivo = ModeloRecomendacion.path+"tiendas.csv"
        dataSetTiendas = pd.read_csv(rutaArchivo,delimiter=",", index_col=0, decimal=",")
        return dataSetTiendas.loc[dataSetTiendas['nombre'].str.contains(nombre)==True]
    
    @staticmethod
    def buscarTienda(codigoTienda):
        rutaArchivo = ModeloRecomendacion.path+"tiendas.csv"
        dataSetTiendas = pd.read_csv(rutaArchivo,delimiter=",", index_col=0, decimal=",")
        return dataSetTiendas.loc[dataSetTiendas['codigo_proveedor'] == int(codigoTienda)]