import json
import bson
from flask import jsonify, Blueprint,request
from api.recomendacion_arboles.modelo import ModeloArbolesEstrategias
from api.recomendacion_arboles.logica import LogicaArbolesEstrategias


recomendacion_arboles = Blueprint('recomendacion_arboles', __name__)

class ControladorArbolesEstrategias():
    
    @staticmethod
    @recomendacion_arboles.route('/recomendacion_arboles/modelo', methods=['GET'])
    def crearModeloRecomendacion():
        dsEstrategias= ModeloArbolesEstrategias.obtenerEjecucionEstrategias()
        LogicaArbolesEstrategias.crearModeloRecomendacion(dsEstrategias)
        return jsonify({'result':"ok"})
    
    
    @staticmethod
    @recomendacion_arboles.route('/recomendacion_arboles/estrategias', methods=['GET'])
    def recomendarEstrategias():
        dsEstrategias= ModeloArbolesEstrategias.obtenerEjecucionEstrategias()
        modelo,estructuraAtributosModelo = LogicaArbolesEstrategias.crearModeloRecomendacion(dsEstrategias)
        
        listaItems = dsEstrategias["estrategia"].unique()
        atributosPrediccion= ModeloArbolesEstrategias.obtenerAtributosPrediccion()
        estrategias = LogicaArbolesEstrategias.recomendarEstrategiasTop(modelo,atributosPrediccion,listaItems,estructuraAtributosModelo)
        return jsonify({'result':"ok", "datos":estrategias})
    