import pandas
from api import app
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
from sklearn import tree
from api.comunes.normalizacion import Normalizacion
from api.comunes.utilidades import Utilidades
import graphviz
from sklearn.cross_validation import cross_val_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
import numpy

class LogicaArbolesEstrategias():
    path = "../datos/"
    
    @staticmethod
    def normalizarCamposEstrategias(dsAtributos):
        camposCategorias = ["objetivo","inversion","nivel_acercamiento","temporada","crecimiento_ventas","devoluciones","antiguedad","estrategia"]
        
        dsAtributos = Normalizacion.normalizarAtributosCategorias(dsAtributos,camposCategorias)
        return dsAtributos
    
    @staticmethod
    def crearModeloRecomendacion(dsEstrategias):
        dsEstrategias = LogicaArbolesEstrategias.normalizarCamposEstrategias(dsEstrategias)
        
        #separo los atributos de las clases
        dsAtributosEstrategias  = dsEstrategias.drop('calificaciones', axis=1)
        dsClasesEstrategias = dsEstrategias['calificaciones']
        
        modelo = LogicaArbolesEstrategias.crearModelo()
        
        #dsClasesEstrategias,encoder = Normalizacion.preprocessEtiquetas(dsClasesEstrategias)
        
        clases = dsClasesEstrategias.unique()
        
        #la estructura del los campos cuales se realiza el aprendizaje
        estructuraAtributosModelo = dsAtributosEstrategias.columns
        
        #divido el dataset en datos de entrenamiento y de prueba
        dsAtributosEntrenamiento, dsAtributosPrueba, dsClasesEntrenamiento, dsClasesPrueba = train_test_split(dsAtributosEstrategias, dsClasesEstrategias, test_size=0.20)
        
        LogicaArbolesEstrategias.entrenarModelo(modelo,dsAtributosEntrenamiento,dsClasesEntrenamiento)
        print("Mejores parametros")
        #print(modelo.best_params_)
        
        LogicaArbolesEstrategias.evaluarModelo(modelo,dsAtributosPrueba,dsClasesPrueba)
        #LogicaArbolesEstrategias.visualizarArbol(modelo,dsAtributosEstrategias,clases)
        
        return modelo,estructuraAtributosModelo
        
    @staticmethod
    def crearModelo():
        #modelo = DecisionTreeClassifier(criterion='entropy')
        modelo = RandomForestClassifier(bootstrap = False, min_samples_leaf= 1,
            n_estimators = 600, max_features= 'sqrt', min_samples_split= 5, 
            max_depth= 60)
        
        #n_estimators = [int(x) for x in numpy.linspace(start = 200, stop = 2000, num = 10)]
        #max_features = ['auto', 'sqrt']
        #max_depth = [int(x) for x in numpy.linspace(10, 110, num = 11)]
        #max_depth.append(None)
        #min_samples_split = [2, 5, 10]
        #min_samples_leaf = [1, 2, 4]
        #bootstrap = [True, False]
        
        #random_grid = {'n_estimators': n_estimators,
              # 'max_features': max_features,
               #'max_depth': max_depth,
               #'min_samples_split': min_samples_split,
               #'min_samples_leaf': min_samples_leaf,
               #'bootstrap': bootstrap}
        
        #modelo = RandomizedSearchCV(estimator = modelo, param_distributions = random_grid, n_iter = 100, cv = 3, verbose=2, random_state=42, n_jobs = -1)
        
        return modelo
    
    @staticmethod
    def entrenarModelo(modelo,dsAtributosEntrenamiento,dsClasesEntrenamiento):
        modelo.fit(dsAtributosEntrenamiento,dsClasesEntrenamiento)
        
    @staticmethod   
    def evaluarModelo(modelo,dsAtributosPrueba,dsClasesPrueba):
        #realizo la prediccion con los datos de pruebas
        dsClasesPrediccion = modelo.predict(dsAtributosPrueba)
        #vuelvo los datos a codigos, a como estaban originalmente
        #dsClasesPrueba = Normalizacion.transformarCategorias(dsClasesPrueba)
        #dsClasesPrediccion = Normalizacion.transformarCategorias(dsClasesPrediccion)
        
        print("Clases prueba:")
        print(dsClasesPrueba)
        print("Clases prediccion:")
        print(dsClasesPrediccion)
        
        #creo la matriz de confusion
        print(confusion_matrix(dsClasesPrueba, dsClasesPrediccion))
        print(classification_report(dsClasesPrueba, dsClasesPrediccion))
    
    @staticmethod  
    def visualizarArbol(modelo,dsAtributos,nombreClases):
        atributos = list(dsAtributos.columns)
        with open(LogicaArbolesEstrategias.path+"modelo_tree.dot", 'w') as f:
            f = tree.export_graphviz(modelo, out_file=f,class_names=nombreClases,
            feature_names=atributos, filled=True)
        
    @staticmethod
    def recomendarEstrategiasTop(modelo,atributosPrediccion,listaItems,estructuraAtributosModelo):
        dsEstrategias = Utilidades.agregarListaDataFrame(atributosPrediccion,listaItems,"estrategia")
        dsEstrategias = LogicaArbolesEstrategias.normalizarCamposEstrategias(dsEstrategias)
        
        #creo un dataframe con la estructura del modelo del aprendizaje luego de la normalizacion
        dataframeResultante = pandas.DataFrame(columns=estructuraAtributosModelo)
        for columna in dsEstrategias.columns:
            dataframeResultante[columna] = dsEstrategias[columna]
        
        dataframeResultante = dataframeResultante.fillna(0)
        dsEstrategias = dataframeResultante
        
        topPredicciones = modelo.predict(dsEstrategias)
        
        result = []
        indice = 0
        for prediccion in topPredicciones:
            result.append({'estrategia':listaItems[indice],'calificacion':prediccion})
            indice = indice + 1
            
        result = sorted(result, key=lambda k: k['calificacion'], reverse=True) 
        return result[:5]
    