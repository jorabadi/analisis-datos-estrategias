import pandas as pd


class ModeloArbolesEstrategias():
    path = "../datos/"
    
    @staticmethod
    def obtenerEjecucionEstrategias():
       rutaArchivo = ModeloArbolesEstrategias.path+"estrategias_enfoque_vf.csv"
       return pd.read_csv(rutaArchivo,delimiter=",", index_col=0)
   
    @staticmethod
    def obtenerAtributosPrediccion():
       rutaArchivo = ModeloArbolesEstrategias.path+"estrategias_prediccion.csv"
       return pd.read_csv(rutaArchivo,delimiter=",", index_col=0)