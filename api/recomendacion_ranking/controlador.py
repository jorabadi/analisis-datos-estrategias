import json
import bson
from flask import jsonify, Blueprint,request
from api.recomendacion_ranking.logica import LogicaEstrategias
from api.recomendacion_ranking.modelo import ModeloEstrategias

recomendacion_ranking = Blueprint('recomendacion_ranking', __name__)

class ControladorEstrategias():
    path = "../tmp/modelo_recomendacion_ranking_pesos.h5"
    
    @staticmethod
    @recomendacion_ranking.route('/recomendacion_ranking/modelo', methods=['GET'])
    def actualizarModeloRecomendacion():
        
        dsEstrategias = ModeloEstrategias.obtenerEjecucionEstrategias()
        modelo = LogicaEstrategias.actualizarModeloRecomendacion(dsEstrategias)
        ModeloEstrategias.guardarModeloRecomendacion(modelo,ControladorEstrategias.path)
        return jsonify({'result':"ok"})
    
    @staticmethod
    @recomendacion_ranking.route('/recomendacion_ranking/modelo/evaluar', methods=['GET'])
    def evaluarModeloRecomendacion():
        dsEstrategias = ModeloEstrategias.obtenerEjecucionEstrategias()
        red = ModeloEstrategias.cargarModeloRecomendacion(ControladorEstrategias.path)
        LogicaEstrategias.evaluarModeloRecomendacion(red,dsEstrategias)
        
        return jsonify({'result':"ok"})
    
    @staticmethod
    @recomendacion_ranking.route('/recomendacion_ranking/ejecucion', methods=['POST'])
    def insertarEjecucionEstrategia():
        dsEjecucionEstrategia = json.loads(request.data)
        ModeloEstrategias.insertarEjecucionEstrategia(dsEjecucionEstrategia)
        
    @staticmethod
    @recomendacion_ranking.route('/recomendacion_ranking', methods=['GET'])
    def obtenerEjecucionEstrategias():
        listaEstrategias = ModeloEstrategias.obtenerEjecucionEstrategias()
        return jsonify({'result':"ok"})