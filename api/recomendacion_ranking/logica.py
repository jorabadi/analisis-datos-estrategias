from api import app
from api.comunes.normalizacion import Normalizacion
from flask import jsonify, Blueprint
import h5py
from keras.models import Sequential
from keras.layers import Dense
from numpy import array
from sklearn.cross_validation import train_test_split
from keras.optimizers import Adam
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
import numpy
import pandas

class LogicaEstrategias():
    
    @staticmethod
    def normalizarCamposEstrategias(dataframe):
        camposFecha = ["fecha_inicio","fecha_fin"]
        camposHora = ["hora_inicio","hora_fin"]
        camposCategorias = ["temporada","mundo_pertenece","tipo_producto","tipo_estrategia","calificacion"]
        
        camposAtributos = ["fecha_inicio","fecha_fin","hora_inicio","hora_fin","ventas_mes","tiene_oferta","ventas_penultimo_mes","ventas_ultimo_mes","crecimiento_ventas",
        "inversion_plataforma","inversion_tienda","registros","pines_mes"]
        
        dataframe = Normalizacion.normalizarCamposFechas(dataframe,camposFecha)
        dataframe = Normalizacion.normalizarCamposHoras(dataframe,camposHora)
        dataframe = Normalizacion.normalizarCamposCategorias(dataframe,camposCategorias)
        dataframe = Normalizacion.preProcesarAtributos(dataframe,camposAtributos)
        return dataframe
    
    @staticmethod
    def obtenerDatosEntrenamientoPrueba(colEstrategias,numVarEntradas):
         # semilla para generar los calculos
        seed = 7
        numpy.random.seed(seed)
        dfEstrategias = Normalizacion.transformarColeccionToDataFrame(colEstrategias)
        dfEstrategias = LogicaEstrategias.normalizarCamposEstrategias(dfEstrategias)
        dsEstrategias = dfEstrategias.values
        
        #X son los parametros, Y son las categorias
        #obtengo las columnas de atributos y de categorias
        dsAtributos = dsEstrategias[:,0:numVarEntradas]
        dsCategorias = dsEstrategias[:,numVarEntradas-1]
        
       
        #creo los datos para probar el modelo de recomendacion generado
        dsAtributos_train, dsAtributos_test, dsCategorias_train, dsCategorias_test = train_test_split(dsAtributos, dsCategorias, test_size=0.2, random_state=seed)
        
        return dsAtributos_train, dsAtributos_test, dsCategorias_train, dsCategorias_test
    
    @staticmethod
    def actualizarModeloRecomendacion(colEstrategias):
        numVarEntradas = 19
        numVarSalidas = 20
        
        #creo los datos para probar el modelo de recomendacion generado
        dsAtributos_train, dsAtributos_test, dsCategorias_train, dsCategorias_test = LogicaEstrategias.obtenerDatosEntrenamientoPrueba(colEstrategias,numVarEntradas)
        
        dsCategorias_train = Normalizacion.preProcesarClases(dsCategorias_train,numVarSalidas)
        dsCategorias_test = Normalizacion.preProcesarClases(dsCategorias_test,numVarSalidas)
        
        #creo la red neuronal se calculo las entradas
        red = LogicaEstrategias.crearRedNeuronal(numVarEntradas,numVarSalidas)
        # Fit the model
        red.fit(dsAtributos_train, dsCategorias_train, epochs=32, batch_size=64,validation_data=(dsAtributos_train, dsCategorias_train))
        
        #LogicaEstrategias.evaluarModeloRecomendacion(red,dsAtributos_test,dsCategorias_test)
        return red
    
    @staticmethod
    def evaluarModeloRecomendacion(red,colEstrategias):
        numVarEntradas = 19
        numVarSalidas = 20
        
        #creo los datos para probar el modelo de recomendacion generado
        dsAtributos_train, dsAtributosTest, dsCategorias_train, dsCategoriasTest = LogicaEstrategias.obtenerDatosEntrenamientoPrueba(colEstrategias,numVarEntradas)
        
        predicciones = red.predict(dsAtributosTest)
        # evaluate the model
        scores = red.evaluate(dsAtributosTest, dsCategoriasTest)
        print("%s: %.2f%%" % (red.metrics_names[1], scores[1]*100))
        
        dsCategoriasTest = Normalizacion.transformarCategorias(dsCategoriasTest)
        dsCategoriasPrediccion = Normalizacion.transformarCategorias(predicciones)
        
        print("Categorias prediccion:")
        print(dsCategoriasPrediccion)
        
        LogicaEstrategias.crearMatrizConfusion(dsCategoriasTest,dsCategoriasPrediccion)
        
    @staticmethod
    def crearMatrizConfusion(clasesReales, clasesPredictas):
        #matrix de confusion
        matrizConfusion =  confusion_matrix(clasesReales, clasesPredictas)
        print("Matriz confusion: ")
        print(matrizConfusion)
        tp = float(matrizConfusion[0][0])/numpy.sum(matrizConfusion[0])
        tn = float(matrizConfusion[1][1])/numpy.sum(matrizConfusion[1])
        print("Tp: ")
        print(tp)
        print("Tn: ")
        print(tn)
        
        clasificacion = classification_report(clasesReales, clasesPredictas)
        print("Clasificacion: ")
        print(clasificacion)
            
    @staticmethod
    def crearRedNeuronal(numVarEntradas,numVarSalidas):
        #9 columnas = parametros de entrada
        #5 categorias = nodos de salida
        red = Sequential()
        red.add(Dense(numVarEntradas, input_dim = numVarEntradas, activation='relu'))
        red.add(Dense(250,  activation='relu'))
        red.add(Dense(numVarSalidas,  activation='softmax'))
        adam = Adam(lr=0.001)
        red.compile(loss= 'categorical_crossentropy', optimizer=adam, metrics = ['accuracy'])
        return red
    
   