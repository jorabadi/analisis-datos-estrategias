import json
from api.database import coneccionDb
from collections import OrderedDict
from keras.models import load_model
from keras.models import model_from_json

class ModeloEstrategias():
    @staticmethod
    def obtenerEjecucionEstrategias():
        camposSeleccion = { "_id":0,
            "fecha_inicio":1,"fecha_fin":1,"temporada.id":1,
            "hora_inicio":1,"hora_fin":1,"ventas_mes":1,
            "tiene_oferta":1,"ventas_penultimo_mes":1,"ventas_ultimo_mes":1,"crecimiento_ventas":1,
            "inversion_plataforma":1,
            "inversion_tienda":1,"mundo_pertenece.id":1,"pines_mes":1,"tipo_producto.id":1,
            "tipo_estrategia.id":1,"registros":1,"calificacion.id":1,"estrategia.id":1
        }
        coleccion = coneccionDb.ejecucion_estrategias
        colEstrategias = coleccion.find({},camposSeleccion)
        listaEstrategias = []
        for cEstrategia in colEstrategias:
            estrategia = OrderedDict()
            estrategia["fecha_inicio"] = cEstrategia["fecha_inicio"]
            estrategia["fecha_fin"] = cEstrategia["fecha_fin"]
            estrategia["temporada"] = cEstrategia["temporada"]["id"]
            estrategia["hora_inicio"] = cEstrategia["hora_inicio"]
            estrategia["hora_fin"] = cEstrategia["hora_fin"]
            estrategia["ventas_mes"] = cEstrategia["ventas_mes"]
            estrategia["tiene_oferta"] = cEstrategia["tiene_oferta"]
            estrategia["ventas_penultimo_mes"] = cEstrategia["ventas_penultimo_mes"]
            estrategia["ventas_ultimo_mes"] = cEstrategia["ventas_ultimo_mes"]
            estrategia["crecimiento_ventas"] = cEstrategia["crecimiento_ventas"]
            estrategia["inversion_plataforma"] = cEstrategia["inversion_plataforma"]
            estrategia["inversion_tienda"] = cEstrategia["inversion_tienda"]
            estrategia["mundo_pertenece"] = cEstrategia["mundo_pertenece"]["id"]
            estrategia["pines_mes"] = cEstrategia["pines_mes"]
            estrategia["tipo_producto"] = cEstrategia["tipo_producto"]["id"]
            estrategia["tipo_estrategia"] = cEstrategia["tipo_estrategia"]["id"]
            estrategia["registros"] = cEstrategia["registros"]
            estrategia["calificacion"] = cEstrategia["calificacion"]["id"]
            estrategia["estrategia"] = cEstrategia["estrategia"]["id"]
            listaEstrategias.append(estrategia)
        return listaEstrategias
    
    @staticmethod
    def insertarEjecucionEstrategia(dsEjecucionEstrategia):
        coneccionDb.ejecucion_estrategias.insert_one(dsEjecucionEstrategia)
    
    @staticmethod  
    def guardarModeloRecomendacion(modelo,path):
        modelo.save_weights(path)
        modeloJson = {"modelo":modelo.to_json()} 
        coneccionDb.modelo.insert_one(modeloJson)
        
    @staticmethod  
    def cargarModeloRecomendacion():
        coleccion = coneccionDb.ejecucion_estrategias
        colModelo = coleccion.find({},{"_id":0,"modelo":1,"pesos":1})
        modelo = model_from_json(colModelo[0]["modelo"])
        modelo.set_weights(json.loads(colModelo[0]["pesos"]))
        return modelo