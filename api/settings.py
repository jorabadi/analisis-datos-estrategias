# Flask settings
FLASK_SERVER_HOST = '127.0.0.1'
FLASK_SERVER_PORT = 8000
FLASK_DEBUG = True  # Do not use debug mode in production

# Mongodb settings
MONGODB_SERVER_HOST = 'localhost'
MONGODB_SERVER_PORT = 27017
MONGODB_DB = 'vf_analitica'

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False
